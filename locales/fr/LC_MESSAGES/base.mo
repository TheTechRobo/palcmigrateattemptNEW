��    �      ,    <      X     Y  �  a       5  #  D   Y  &   �     �     �  "   �       "   .  K   Q  $   �  u   �  �   8  7   )     a     ~  Q   �     �  y   �  K   i     �     �     �  )   �  
   !     ,  Q  :  ]   �  \   �      G  &   h     �     �     �     �     �            7   4     l     �     �  
   �     �  2   �  �       �  �   �     �  *   �  &   �  )      %   G      m   1   �       �      �      �      	!     "!  .   ?!  .   n!  )   �!  �   �!  +   V"     �"     �"     �"     �"  ,   �"     �"     #     #  )   "#     L#  D   _#     �#  ~   �#  3   5$  �   i$  Z   �$  
   S%  _   ^%     �%     �%  "   �%     &     "&     1&     ?&     W&  P   h&  L   �&     '  7   '      W'  $   x'  !   �'  #   �'  %   �'  !   	(  %   +(  5   Q(  (   �(  5   �(      �(  *   )  &   2)  &   Y)  &   �)  %   �)  ,   �)  /   �)  .   **  &   Y*  '   �*  s   �*     +     1+  )   O+  "   y+  +   �+      �+  $   �+  (   ,  (   7,  (   `,  &   �,  "   �,  G   �,  )   -  *   E-     p-  )   �-  +   �-  %   �-  )   .     2.  k   Q.  %   �.  -   �.  A   /  "   S/     v/  &   �/     �/  t   �/     D0     T0     Y0     h0     v0     �0     �0     �0     �0  0   �0     �0     �0     �0      1     1     1     &1     .1     31     :1     ?1     C1     F1     M1     ]1     a1     �1     �1     �1  ,   �1     �1     �1     2     .2     I2     a2  -   }2     �2     �2     �2     �2  $   3  '   *3     R3  i  T3  	   �4  ,  �4     �6  �  7  /   �8  &   �8     9     *9  /   B9  )   r9  -   �9  X   �9  3   #:  �   W:  `  �:  J   E<  *   �<     �<  [   �<     0=  �   E=  \   �=     ,>  "   @>     c>  7   ~>     �>     �>  H  �>  q   )A  l   �A  )   B  )   2B     \B  )   xB     �B     �B     �B     �B     �B  M   C  .   TC  	   �C     �C     �C  "   �C  7   �C  �   D  4  �D  �   F      �F  &   G  4   CG  5   xG  3   �G     �G  7   �G     ,H     @H     `H  !   yH  (   �H  3   �H  L   �H  9   EI  �   I  0   BJ     sJ     �J     �J     �J  7   �J      K     K     $K  ?   4K     tK  8   �K     �K  �   �K  9   nL  �   �L  z   NM     �M  q   �M  #   KN      oN  -   �N     �N     �N     �N  !   �N     O  J   *O  b   uO     �O  6   �O     *P     >P     UP     jP     �P     �P     �P  8   �P  #   Q  6   'Q     ^Q  %   uQ     �Q     �Q     �Q     �Q      R  #   2R     VR      vR  (   �R  x   �R     9S     OS  +   oS     �S     �S     �S     �S     �S     T     -T     KT     cT  J   zT  "   �T  *   �T  +   U  *   ?U     jU  .   �U  !   �U  H   �U  �   V  :   �V  3   �V  `   W  6   |W  *   �W  6   �W     X  �   $X     �X     �X     �X     �X  +   �X     Y     !Y     ;Y     MY  4   UY     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y      Z     Z     "Z  "   'Z     JZ     OZ     bZ  3   ~Z     �Z     �Z  #   �Z     [     3[      Q[  0   r[  )   �[     �[     �[     �[     \     (\     E\        _   4       �   /           )   ]                  .   �   r   t   �       ~       �   �       p           Q   O   �   �      ^   j   2   �   7   *   =   N   m   �   �   W   �   �                   8           �   A              s   C       �   v   $           d       �   T   �   �      �   k               \      ?              �   (   �   �       g   {       +   �   |   [          R   �   f   a   <       �   �   ;           �   �   �   G              P   L   �      :       "      V          �   ,   �   �   6   Y       �   H   �   �       !           D               S                  �   3   �   I   #   �   `           �   �   �       �   y   �       n   '   x   �      �         E   �      Z   X   }   h   �   �   e   l   �      �       w       B   u   i   %   �   �   F       b   @   �       �           1   
       �   >   J       �      c       -       �   0   �   9          	   �      �   5   �       �      q             �   �      �   �           �      K   U      z       &              M      o    ERROR! 
Current list of commands: multiplication, division, addition, square, subtraction, modulo, area, volume, cube, exponents, root, logarithm, memory, interest calculator, fibonacci sequence, percentage calculator, convert temperature, "ord'ing", and convert bases (aka number systems). Type quit to quit.
Bugs? Head on over to https://github.com/thetechrobo/support/
To contribute: go to https://github.com/thetechrobo/python-text-calculator/
 
Error! 
I don't understand your request. Here are the currently supported calculations:
multiplication, division, subtraction, addition, modulo, square, area, volume, cube, power, root, ord, fibonacci, logarithm, memory, percentage calculator, interest calculator, temperature, and base. Sorry for the inconvenience
 
Note that you CAN type `quit' instead of pressing the interrupt key 
Please enter the number to be saved:  
That equals...
%s 
Type in a number:  
Type the first number (greater):  
Type the number to be cubed:  
Why ^D? Why not just type `quit'? 1 - Calculate "What is x% of y?"
2 - Convert a number to percentage.
Type:  1 - Convert temperature units
Type:  1 - Find the remainder of two numbers after division
2 - Use the percentage calculator.
Anything else - Back to menu. 1 - Ontario Sales Tax
2 - Quebec Sales Tax
3 - Yukon, Northwest Territories, Nunavut, and Alberta Sales Tax
4 - BC / Manitoba Sales Tax
5 - New Brunswick / Nova Scotia / Newfoundland / PEI Sales Tax
6 - Saskatchewan Sales Tax
7 - Custom Tax =42 -- the answer to life, the universe, and everything After tax, the price is: 
%s An error occured. An unknown error occured. Please file an Issue at github.com/thetechrobo/support. Choose one:  Converting from a base to decimal (normal).
Example bases:
2 - Binary
8 - Oct
16 - Hexadecimal
Or, type 1 for ord.
Type:  Currently I don't support the root you chose. Hopefully this will change :) Defaulting to yes. Details are in the log. Do not divide by zero! Do you really need a calculator for this? Exponent?  Going back... How many units of time would you like to calculate? 
Essentially, one unit of time could be one month, or one decade. It all depends on what you typed in the rate of interest question: it could be per year, per decade...we didn't ask.
It was up to you to type the correct amount in the rate question.
We have no idea what the rate represented: it could have been that rate per century for all we know.
This calculator wasn't programmed with the ability to track time.
So, with that out of the way, type the amount we should multiply the interest by (aka the amount of units of time).
Type it:  I can't access file basicfunc.py. This file is necessary for proper function of the Software. I can't access the file func.py. This file is necessary for proper function of the Software. I can't heeeaaaarrrrr youuuuuuuu I can't heeeeaaaarrrrrr yooooouuuuuuuu I think you want me to:  I was too lazy to change 7. Invalid input (code 1)
 Invalid response. Is this correct? (Y/n) Loading...............
 Looks like you exited. Now enter the tax percentage without the percent sign:  Number to be rooted? Number to be rooted?  Number to square?  Number: %s OK, enter the original price:  OK, proceeding with the function I thought it was. OPTIONS:
    1 - Farenheit to Celsius
    2 - Celsius to Farenheit
    3 - Farenheit to Kelvin
    4 - Celsius to Kelvin
    5 - Kelvin to Celsius
    6 - Kelvin to Farenheit
Type:  Options:
1 - Cube
2 - Cuboid
3 - Cylinder
4 - Hollow cylinder
5 - Cone
6 - Sphere
8 - Hollow sphere
9 - Triangular prism
10 - Pentagonal prism
11 - Hexagonal prism
12 - Square-based pyramid
13 - Triangular pyramid
14 - Pentagon-based pyramid
15 - Hexagon-based pyramid Options:
1 - Equilateral triangle
2 - Right angle triangle
3 - Acute triangle
4 - Obtuse triangle
5 - Square
6 - Rectangle
8 - Parallelogram
9 - Rhombus
10 - Trapezium
11 - Circle
12 - Semicircle
13 - Circular sector
14 - Ring
15 - Ellipse Original number? Please do not type in a zero as the whole. Please enter the CELSIUS temperature:  Please enter the FAHRENHEIT temperature:  Please enter the KELVIN temperature:  Please enter the first number:  Please enter the number to be converted from %s:  Please enter the second number:  Please type an integer Please type one:  Press Control-C to stop. Press any key to continue... Proceeding with the function I thought it was. Square root or cube root? (square/cube)
Type:  Starting fibonacci sequence. Please wait. TIP: If you got no answer, it might be that it was a Unicode character that it can't render. E.g. \x06 would just be a blank space, like so:  Thanks for using Palc's FIBONACCI function! That equals... That equals...
%s That equals.....
%s That equals: The " CUBE TWICE " feature was discontinued. The answer is: 
%s The area is:  The area is: %s The logarithm you typed is not available. The result is: 
%s The second number entered is greater than the first number (code 2)
 The volume is: %s There was an error reading the file. Did you save the number by using the save function? Did you accidentally rename the file? There was an unknown issue dividing your Numbers... This is the memory function.
It will save a number into a file that can be used later with Palc... Or you can just read it with a text editor. This is the remember function.
It will read a number that was previously stored in a file. Try again. Try different wording. Or, if you want that calculation choice to be made right, file a ticket. Type in the character to ord:  Type the original number:  Type the second number (smaller):  Type:  Unknown Error! Using base 10 Using natural logarithm Welcome to Palc! What base would you like to use? 
Currently supported: 10 (base 10), e (natural) What calculation do you wish to do? (Type `?' for a list of commands)
Type:  What is the PERCENTAGE?  What is the angle of the circular sector *in degrees*?  What is the height of the cone?  What is the height of the cylinder?  What is the height of the prism?  What is the height of the pyramid?  What is the height of the rectangle?  What is the height of the shape?  What is the height of the trapezium?  What is the length of the 1st set of parallel sides?  What is the length of the 2nd diagonal?  What is the length of the 2nd set of parallel sides?  What is the length of the base?  What is the length of the first diagonal?  What is the length of the first side?  What is the length of the major axis?  What is the length of the minor axis?  What is the length of the rectangle?  What is the length of the side of the base?  What is the length of the side of the hexagon?  What is the length of the side of the square?  What is the length of the third side?  What is the number that would be 100%?  What is the number that you want to convert to percentage (e.g. this number out of the number that would be 100%)?  What is the number?  What is the original number?  What is the original price (before tax)?  What is the radius of the circle?  What is the radius of the circular sector?  What is the radius of the cone?  What is the radius of the cylinder?  What is the radius of the hollow space?  What is the radius of the inner circle?  What is the radius of the outer circle?  What is the radius of the semicircle?  What is the radius of the sphere?  What is the rate of interest in percentage (without the percent sign)?  What length is the base of the triangle?  What length is the breadth of the cuboid?  What length is the cuboid?  What length is the height of the cuboid?  What length is the height of the triangle?  What length is the side of the cube?  What length is the side of the triangle?  What slot number did you use?  What slot would you like to use? (Hint: you can use any integer you want as long as you remember it)
Type:  Would you like to do it again (Y/n)?  Would you like to file a ticket? (Y/n)
Type:  Would you like to set the memory or recall? (set / recall)
Type:  You did not type an answer.
Abort. You did not type answer. Abort. You didn't type a valid answer. Abort. You speak quietly You typed in an invalid integer or float. Or maybe the program needs debugging. Either way, it's a pretty big error. add two numbers area calculate area calculate tax convert number systems cube cube a number divide a number exit find the remainder of two numbers after division going back. help interest log multiply a number ord a character percent quit recall root set sq square square a number sub subtract a number from a number tax that equals: use the converter functions use the equals function (completely useless) use the exponent function use the fibonacci calculator use the interest calculator use the logarithm function use the memory function use the percentage function use the root function (opposite of exponents) use the temperature converter use the volume calculator vol what is the ORIGINAL NUMBER?  what is the height of the cylinder?  what is the length of the second side?  y Project-Id-Version: Palc
PO-Revision-Date: 2020-08-11 14:26-0400
Last-Translator: ittussarom
Language-Team: 
Language: fr_CA
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _
X-Poedit-SearchPath-0: .
 ERREUR ! 
Liste des commandes : multiplication, division, soustraction, addition, modulo, carré, aire, volume, cube, exponents, racines, fibonacci, ord, logarithme, mémoire, calculatrice pourcentage, calculatrice d'intérêt, température, et base (convertir les nombres entre différentes bases). Tapez quitter pour quitter.
Bugs ? Rendez-vous sur https://github.com/thetechrobo/support
Si vous souhaitez contribuer : https://github.com/thetechrobo/python-text-calculator
(IMPORTANT : N'AJOUTEZ PAS LES ACCENTS AUX COMMANDES, AUTREMENT CELA NE FONCTIONNERA PAS)
 
Une erreur est survenue ! 
Je ne sais pas ce que vous voulez faire. Voici une liste des calculs supportés :
multiplication, division, soustraction, addition, modulo, carré, aire, volume, cube, exponents, racines, fibonacci, ord, logarithme, mémoire, calculatrice pourcentage, calculatrice d'intérêt, température, et base. Désolé pour le dérangement : /
(IMPORTANT : N'AJOUTEZ PAS LES ACCENTS AUX COMMANDES, AUTREMENT CELA NE FONCTIONNERA PAS)
 
Vous pouvez saisir `quitter' plutôt que ^C... 
Saisissez le nombre à enregistrer :  
Le résultat est...
%s 
Saisissez un nombre :  
Saisissez le premier nombre (le plus grand) :  
Saisissez le nombre à mettre au cube :  
Pourquoi ^D ? Pourquoi pas juste `quitter' ? 1 - Calculer "Combien font x % de y ?"
2 - Convertir un nombre en pourcentage.
Saisie :  1 - Convertir les unités de température
Saisie :  1 - Calculer le reste de deux numéros après la division (le modulo)
2 - Utiliser la calculatrice pourcentage
Autre chose - Retour au menu. 1 - Taxe de vente de l'Ontario 
2 - Taxe de vente du Québec
3 - Taxe de vente du Yukon, des Territoires du Nord-Ouest, du Nunavut et de l'Alberta 
4 - Taxe de vente de la Colombie-Britannique / Manitoba 
5 - Taxe de vente du Nouveau-Brunswick / Nouvelle-Écosse / Terre-Neuve / Î.-P.-É. 
6 - Taxe de vente de la Saskatchewan 
7 - Taxe personnalisée =42 -- la réponse de la grande question sur la vie, l'univers et le reste Après ajout de la taxe, le prix est : 
%s Une erreur est survenue. Une erreur s'est produite. Veuillez soumettre un billet sur github.com/thetechrobo/support. Choisissez-en une :  Conversion d'une base en décimal (normal). 
Exemples de bases: 
2 - Binaire 
8 - Oct
16 - Hexadécimal 
Saisissez 1 pour ord. 
Saisie :  Pour le moment je ne supporte pas la racine que vous voulez. J'espère que ça va changer :) Par défaut à oui. Les détails sont dans le journal. Ne divisez pas par zéro ! Vous avez vraiment besoin d'une calculatrice pour ça ? Saisissez l'exposant :  Retour au menu... Combien d'unités de temps souhaitez-vous calculer ? 
Une unité de temps peut être un mois, ou 10 ans. Cela dépend de ce que vous avez saisi comme taux de l'intérêt : il peut être mensuel, annuel... Nous n'avons pas demandé.
C'était à vous de saisir le taux adéquat. 
Nous ignorons la périodicité de ce taux : ça pourrait même être un taux par siècle.
Cette calculatrice n'a pas été programmée avec la fonctionnalité de mesurer le temps.
Suite à ces précisions, saisissez la durée pendant laquelle appliquer l'intérêt (le nombre d'unités de temps).
Saisie :  Impossible d'accéder au fichier basicfunc.py. Ce fichier est nécessaire pour le bon fonctionnement du logiciel. Impossible d'accéder au fichier func.py. Ce fichier est nécessaire pour le bon fonctionnement du logiciel. Je ne peux pas enteeeeennnnndre toiiiiiii Je ne peux pas enteeeeennnnndre toiiiiiii Je pense que vous voulez :  J’étais trop paresseux pour changer 7. Saisie invalide (code 1)
 Saisie invalide. Est-ce correct ? (O/n) Chargement...
 Vous quittez Palc. Saisissez le montant de la taxe en pourcentage (sans le signe pourcentage) :  Nombre dont vous souhaitez obtenir la racine ? Nombre ?  Nombre à mettre au carré?  Nombre : %s Ok, saisissez le prix d'origine :  Ok, utilisation de la fonction initialement détectée. OPTIONS : 
    1 - Fahrenheit en Celsius
    2 - Celsius en Fahrenheit
    3 - Fahrenheit en Kelvin
    4 - Celsius en Kelvin
    5 - Kelvin en Celsius
    6 - Kelvin en Fahrenheit
Saisie :  Options:
1 - Cube
2 - Parallélépipède
3 - Cylindre
4 - Cylindre creux
5 - Cône
6 - Sphère
8 - Sphère creuse
9 - Prisme triangulaire
10 - Prisme pentagonal
11 - Prisme hexagonal
12 - Pyramide a base carre
13 - Pyramide a base triangulaire
14 - Pyramide a base pentagonale
15 - Pyramide a base hexagonale Options :
1 - Triangle équilatéral
2 - Triangle rectangle
3 - Triangle aigu
4 - Triangle obtu
5 - Carré
6 - Rectangle
8 - Parallélogramme
9 - Losange
10 - Trapèze
11 - Cercle
12 - Demi-cercle
13 - Secteur circulaire
14 - Anneau
15 - Ellipse Saisissez le nombre d'origine :  Le total ne peut être égal à zéro. Veuillez entrer la température en degré CELSIUS :  Veuillez entrer la température en degré FARENHEIT:  Veuillez entrer la température en degré KELVIN :  Premier nombre ?  Veuillez entrer le nombre à convertir depuis une %s :  Deuxième nombre ?  Merci d'entrer un nombre entier Choisissez une option :  Appuyez sur Ctrl-C pour arrêter. Appuyer sur une touche pour continuer… Utilisation de la fonction initialement détectée. Racine carré ou cubique ? (carre/cube, n'ajouter pas les accents)
Saisie :  Démarrage de la séquence Fibonacci. Veuillez patienter. CONSEIL : si vous n'avez pas eu de résultat, il est possible qu'il s'agisse d'un caractère Unicode qui ne peux pas être affiché. Par exemple \x06 serait juste une espace vide, comme ceci :  Merci d'utiliser la fonction Fibonacci de Palc ! Le résultat est... Le résultat est...
%s Le résultat est...
%s Le résultat est : La fonctionnalité "CUBE DEUX FOIS" a été supprimée. Le résultat est :
%s L'aire est :  L'aire est : %s Le logarithme que vous avez sélectionné n'est pas disponible. L'aire est : 
%s Le second nombre est plus grand que le premier (code 2)
 Le volume est : %s Il y a eu une erreur lors de la lecture du fichier. Avez-vous sauvegardé le nombre avec la fonction mémoire ? Avez-vous renommé par accident le fichier ? Il y a eu un problème inconnu en divisant vos nombres... Il s'agit de la fonction mémoire. 
Elle va enregistrer un nombre dans un fichier pour être utilisé plus tard avec Palc ou simplement lu avec un éditeur de texte. Il s'agit de la fonction de lecture de la mémoire.
Elle permet de lire un nombre qui a été enregistré dans un fichier. Essayez encore. Essayez un libellé différent. Ou, si vous voulez que ce choix de calcul soit implémenté, soumettez un billet. Saisissez le caractère à 'ord' :  Saisissez le nombre d'origine :  Saisissez le second nombre (le plus petit) :  Tapez:  Erreur inconnue ! Utilisation de la base 10 Utilisation du logarithme naturel Bienvenue dans Palc ! Quelle base voulez-vous utiliser ? 
Options : 10 (décimal) ou e (naturel) Quel calcul souhaitez-vous effectuer? (Saisissez `?' pour afficher la liste des calculs)
Saisie :  Quel est le POURCENTAGE ?  Quel est l'angle du secteur circulaire *en degrés* ?  Hauteur du cône ?  Hauteur du cylindre ?  Hauteur du prisme ?  Hauteur de la pyramide ?  Hauteur du rectangle ?  Hauteur de la forme ?  Hauteur du trapèze ?  Longueur de la première paire de côtés parallèles ?  Longueur de la seconde diagonale ?  Longueur de la seconde paire de côtés parallèles ?  Longueur de la base ?  Longueur de la première diagonale ?  Longueur du premier côté ?  Longueur de l'axe principal ?  Longueur de l'axe secondaire ?  Longueur du rectangle ?  Longueur du côté de la base ?  Longueur du côté de l'hexagone ?  Longueur du côté du carré ?  Longueur du troisième côté ?  Quelle est le nombre qui serait 100 % ?  Quel est le nombre que vous souhaitez convertir en pourcentage (par exemple, ce nombre sur le nombre qui serait 100%) ?  Quel est le nombre ?  Quel est le nombre d'origine ?  Quelle est le prix d'origine (hors taxe) ?  Rayon du cercle ?  Rayon du secteur circulaire ?  Rayon du cône ?  Rayon du cylindre ?  Rayon de l'espace creux ?  Rayon du cercle intérieur ?  Rayon du cercle extérieur ?  Rayon du demi-cercle ?  Rayon de la sphère ?  Quel est le taux d'intérêt en pourcentage (sans le signe pourcentage) ?  Longueur de la base du triangle ?  Largeur du parallélépipède rectangle ?  Longueur du parallélépipède rectangle ?  Hauteur du parallélépipède rectangle ?  Hauteur du triangle ?  Quelle est le longueur de l'arrête du cube ?  Longueur du côté du triangle ?  Quel est le numéro de l'emplacement mémoire que vous souhaitez lire ?  Quel emplacement mémoire souhaitez-vous utiliser ? (Vous pouvez utiliser n'importe quel nombre entier tant que vous vous en souvenez)
Saisie :  Souhaitez-vous exécuter de nouveau cette fonction (O/n)?  Souhaitez-vous soumettre un billet? (O/n)
Saisie :  Souhaitez-vous écrire ou lire la mémoire? (ecrire / lire, n'ajouter pas les accents)
Saisie :  Vous n'avez pas saisi une réponse valide. Annulation. Vous n'avez pas saisi une réponse valide. Vous n'avez pas saisi une réponse valide. Annulation. Tu parles doux Soit vous avez entré un nombre invalide, soit le logiciel a besoin d'être débogué. En tout cas, il s'agit d'une une erreur importante. additionne deux nombres aire calculer une aire calculer une taxe convertir les systèmes de bases numérique cube élever un nombre au cube diviser un nombre quitter trouver le reste de deux numéros après la division retour au menu... aide interet log multiplier un nombre "ord" un caractère pourcent quitter lire racine ecrire carre carre élever un nombre au carré sous soustraire un nombre par un nombre taxe le résultat est : utiliser les convertisseurs utiliser la fonction égale (complètement inutile) utiliser la fonction exposant utiliser la fonction fibonacci utiliser le calculateur d'intérêt utiliser la fonction logarithme utiliser la fonction mémoire utiliser la fonction pourcentage utiliser la fonction racine (opposé d'exposant) utiliser le convertisseur de température utiliser la calculatrice volume vol Quel est le nombre d'origine ?  hauteur du cylindre ?  longueur du second côté ?  o 